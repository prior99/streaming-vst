use std::sync::mpsc::Sender;
use vst::buffer::AudioBuffer;

/// Will provide accept the DAW's pcm data and provide it to the streaming client.
pub struct PcmProvider {
    /// A channel to the thread handling the websocket communication and encoding.
    tx: Sender<Vec<f32>>,
}

impl PcmProvider {
    pub fn new(tx: Sender<Vec<f32>>) -> PcmProvider {
        info!("Created new PcmProvider.");
        PcmProvider { tx }
    }

    fn interleave(buffer: &mut AudioBuffer<f32>) -> Vec<f32> {
        let input_count = buffer.input_count();
        let samples = buffer.samples() * input_count;
        let mut interleaved = vec![0f32; samples];
        let mut channel_index = 0;
        for (input_buffer, _) in buffer.zip() {
            let mut sample_index = 0;
            for input_sample in input_buffer.iter() {
                interleaved[sample_index * input_count + channel_index] = *input_sample;
                sample_index += 1;
            }
            channel_index += 1;
        }
        interleaved
    }

    pub fn process_pcm(&mut self, buffer: &mut AudioBuffer<f32>) {
        let data = PcmProvider::interleave(buffer);
        trace!("Sending {} samples of interleaved PCM data to streaming client.", data.len());
        if let Err(error) = self.tx.send(data) {
            error!("Error communicating with thread: {}", error);
        }
    }
}
