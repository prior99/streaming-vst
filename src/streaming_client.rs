use opus::{Encoder, Channels, Application};
use std::sync::mpsc::Receiver;
use std::error::Error;
use websocket::client::ClientBuilder;
use websocket::client::sync::Client;
use websocket::stream::sync::TcpStream;
use websocket::OwnedMessage;
use samplerate::{Samplerate, ConverterType};

const ENCODE_FRAME_MS: usize = 40;
const RESAMPLE_FRAME_MS: usize = 40;
const SAMPLE_RATE: u32 = 48000;

#[derive(Serialize)]
struct Info {
    pub sample_rate: u32,
    pub channels: usize,
}

/// Client for streaming audio data to the server.
pub struct StreamingClient {
    /// The opus encoder.
    encoder: Encoder,
    /// A buffer containing the currently appended PCM data provided by the DAW.
    resample_buffer: Vec<f32>,
    encode_buffer: Vec<f32>,
    /// A channel to the thread handling the websocket communication.
    rx: Receiver<Vec<f32>>,
    /// A samplerate converter.
    converter: Samplerate,
    websocket_client: Client<TcpStream>,
    sample_rate: u32,
}

impl StreamingClient {
    pub fn new(rx: Receiver<Vec<f32>>, sample_rate: u32, url: String) -> Result<StreamingClient, Box<Error>> {
        let mut websocket_client = ClientBuilder::new(&url)?
            .add_protocol("ogg-opus")
            .connect_insecure()?;
        info!("Sending audio information to server: {}Hz on 2 channels.", SAMPLE_RATE);
        let info = Info {
            sample_rate: SAMPLE_RATE,
            channels: 2,
        };
        let info_json = serde_json::to_string(&info).unwrap();
        websocket_client.send_message(&OwnedMessage::Text(info_json)).unwrap();
        info!("Initializing samplerate converter with high quality profile: {}Hz -> {}Hz on 2 channels.", sample_rate, SAMPLE_RATE);
        let converter = Samplerate::new(ConverterType::SincMediumQuality, sample_rate, SAMPLE_RATE, 2)?;
        info!("Initializing opus encoder with samplerate of {}Hz, stereo and high fidelity profile.", SAMPLE_RATE);
        let encoder = Encoder::new(SAMPLE_RATE, Channels::Stereo, Application::Audio)?;
        Ok(StreamingClient {
            encoder,
            resample_buffer: vec![],
            encode_buffer: vec![],
            rx,
            converter,
            websocket_client,
            sample_rate,
        })
    }

    fn process(&mut self, data: Vec<f32>) -> Result<(), Box<Error>> {
        self.resample_buffer.extend(data);
        let resample_frame_size = self.resample_frame_size();
        while self.resample_buffer.len() >= resample_frame_size {
            let slice: Vec<f32> = self.resample_buffer.drain(0..resample_frame_size).collect();
            // Resample the audio data.
            trace!("Resampling a slice of {} samples PCM data with ratio {}.", slice.len(), self.converter.ratio());
            let resampled = self.converter.process(&slice)?;
            self.encode_buffer.extend(resampled);
        }
        let encode_frame_size = self.encode_frame_size();
        while self.encode_buffer.len() >= self.encode_frame_size() {
            let slice: Vec<f32> = self.encode_buffer.drain(0..encode_frame_size).collect();
            trace!("Encoding a slice of {} samples PCM data.", slice.len());
            let encoded = self.encode(slice)?;
            trace!("Sending a binary frame with length {} to server: {:?}.", encoded.len(), encoded);
            self.websocket_client.send_message(&OwnedMessage::Binary(encoded))?;
        }
        Ok(())
    }

    pub fn run(&mut self) {
        info!("Starting main loop in streaming client thread.");
        loop {
            match self.rx.recv() {
                Ok(data) => {
                    trace!("Received {} samples of PCM data.", data.len());
                    if let Err(error) = self.process(data) {
                        error!("Failed to process PCM data: {}", error);
                    }
                },
                Err(error) => {
                    info!("Failed to receive data from master thread: {}", error);
                    break;
                },
            }
        }
        info!("Streaming client thread shutting down.");
    }

    fn resample_frame_size(&self) -> usize {
        (RESAMPLE_FRAME_MS as f64 * (self.sample_rate as f64 / 1000f64) * 2f64) as usize
    }

    fn encode_frame_size(&self) -> usize {
        (ENCODE_FRAME_MS as f64 * (SAMPLE_RATE as f64 / 1000f64) * 2f64) as usize
    }

    fn encode(&mut self, data: Vec<f32>) -> Result<Vec<u8>, opus::Error> {
        let encode_frame_size = self.encode_frame_size();
        trace!("Attempting to encode vec of size {} with frame size of {}, frame duration of {}ms, {}Hz sample rate and {} channels", data.len(), encode_frame_size, ENCODE_FRAME_MS, SAMPLE_RATE, 2);
        self.encoder.encode_vec_float(&data, encode_frame_size)
    }
}
