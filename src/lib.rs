extern crate dirs;
#[macro_use] extern crate log;
extern crate samplerate;
#[macro_use] extern crate serde_derive;
extern crate simplelog;
#[macro_use] extern crate vst;
extern crate websocket;
extern crate yaml_rust;
extern crate opus;

mod streaming_client;
mod pcm_provider;

use simplelog::*;
use std::fs::File;
use std::io::prelude::*;
use std::error::Error;
use std::thread::{spawn, JoinHandle};
use std::sync::mpsc::channel;
use vst::plugin::{Info, Plugin, Category};
use vst::buffer::{AudioBuffer};
use yaml_rust::yaml::{YamlLoader};
use crate::streaming_client::StreamingClient;
use crate::pcm_provider::PcmProvider;

#[derive(Default)]
struct StreamingPlugin {
    url: Option<String>,
    pcm_provider: Option<PcmProvider>,
    sample_rate: Option<u32>,
    client_thread: Option<JoinHandle<()>>,
}

impl StreamingPlugin {
    fn initialize(&mut self) {
        if self.pcm_provider.is_some() { return; }
        match (self.url.clone(), self.sample_rate) {
            (Some(url), Some(sample_rate)) => {
                info!("Initializing streaming client...");
                let (tx, rx) = channel();
                self.pcm_provider = Some(PcmProvider::new(tx));
                self.client_thread = Some(spawn(move || {
                    match StreamingClient::new(rx, sample_rate, url) {
                        Ok(mut client) => {
                            info!("Client created successfully.");
                            client.run()
                        },
                        Err(error) => error!("Unable to start streaming client: {}", error),
                    };
                }));
            },
            _ => (),
        }
    }

    fn load_config(&mut self, config_path: &str) -> Result<(), Box<Error>> {
        info!("Loading config file {}", config_path);
        let mut file = File::open(config_path)?;
        let mut yaml = String::new();
        file.read_to_string(&mut yaml)?;
        let config = YamlLoader::load_from_str(&yaml)?;
        match config[0]["url"].as_str() {
            Some(url) => {
                info!("Using url {}", url);
                self.url = Some(url.to_string());
            },
            None => { error!("Missing property \"url\" in config file."); }
        };
        Ok(())
    }
}

impl Plugin for StreamingPlugin {
    fn init(&mut self) {
        let config_path = dirs::config_dir().unwrap().join("streamvst.yml").to_str().unwrap().to_string();
        let log_path = dirs::data_dir().unwrap().join("streamvst.log").to_str().unwrap().to_string();
        WriteLogger::init(LevelFilter::Info, Config::default(), File::create(log_path).unwrap()).unwrap();
        info!("streamvst starting up.");
        if let Err(error) = self.load_config(&config_path) {
            error!("Error loading config file: {}", error);
        }
    }

    fn set_sample_rate(&mut self, rate: f32) {
        info!("Using sample rate: {}", rate);
        self.sample_rate = Some(rate as u32);
    }

    fn process(&mut self, buffer: &mut AudioBuffer<f32>) {
        if let Some(pcm_provider) = &mut self.pcm_provider {
            pcm_provider.process_pcm(buffer);
            for (input_buffer, output_buffer) in buffer.zip() {
                for (input_sample, output_sample) in input_buffer.iter().zip(output_buffer) {
                    *output_sample = *input_sample;
                }
            }
        }
    }

    fn get_info(&self) -> Info {
        Info {
            name: "Streaming VST".to_string(),
            unique_id: 10223, // Used by hosts to differentiate between plugins.
            vendor: "Frederick Gnodtke".to_string(),
            parameters: 0,
            category: Category::Analysis,
            version: 0010,
            inputs: 2,
            outputs: 2,
            ..Default::default()
        }
    }

    fn resume(&mut self) {
        if self.url.is_none() { return; }
        info!("Initializing client thread.");
        if self.sample_rate.is_none() {
            warn!("Sample rate not define before first PCM data received. Guessing it to be 44.1kHz.");
            self.sample_rate = Some(44100);
        }
        self.initialize();
    }

    fn suspend(&mut self) {
        info!("Stopping the thread.");
        if let Some(thread) = self.client_thread.take() {
            self.pcm_provider = None;
            if let Err(_) = thread.join() {
                error!("Error occured when joining thread.");
            }
        }
    }
}

plugin_main!(StreamingPlugin);
