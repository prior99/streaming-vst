# streaming-vst

Stream music directly from your DAW to an instance of [streaming-server](https://gitlab.com/prior99/streaming-server).

## Configuration

Place a file `streamvst.yml` into your `%AppData%` directory and add the speak url to a room to it:

```
url: "ws://localhost:8080/speak/87a4d8316b"
```

## Debugging

All relevant logging information will be written into a log file: `%AppData%/streamvst.log`.


## Contributors

 - Frederick Gnodtke
